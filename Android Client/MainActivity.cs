﻿using System.Net.Sockets;
using Android.App;
using Android.OS;
using Android.Widget;

namespace Android_Client
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private readonly SocketManager socket = new SocketManager(2048, 4199);
        private Button connectButton;
        private Button sendButton;
        private TextView status;

        public MainActivity()
        {
            socket.ReceivedMessage += socket_ReceivedMessage;
            socket.ConnectedServer += socket_ConnectedServer;
            socket.HostLost += socket_HostLost;
            socket.HostRefused += socket_HostRefused;
        }

        public void EnterLog(string ms)
        {
            RunOnUiThread(() => status.Text = ms);
        }

        private void socket_HostRefused()
        {
            sendButton.Enabled = false;
            connectButton.Text = "Connect";
            EnterLog("Server has been refused");
        }

        private void socket_HostLost()
        {
            sendButton.Enabled = false;
            connectButton.Text = "Connect";
            Toast.MakeText(this, "Server crashed!", ToastLength.Long);
        }

        private void socket_ConnectedServer()
        {
            EnterLog("Successfully connected");
        }

        private void socket_ReceivedMessage(string Message, TcpClient FromClient)
        {
            EnterLog(Message);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            sendButton = FindViewById<Button>(Resource.Id.sendButton);
            sendButton.Enabled = false;
            connectButton = FindViewById<Button>(Resource.Id.connectButton);

            var sendString = FindViewById<EditText>(Resource.Id.sendString);
            status = FindViewById<TextView>(Resource.Id.status);
            connectButton.Click += delegate
            {
                if (socket.Connected)
                {
                    socket.Dissconnect();
                    connectButton.Text = "Connect";
                    sendButton.Enabled = false;
                }
                else
                {
                    socket.Connect(sendString.Text);
                    connectButton.Text = "Disconnect";
                    sendButton.Enabled = true;
                }
            };
            sendButton.Click += delegate { socket.SendMessageToHost(sendString.Text); };
        }
    }
}