﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Windows_Server
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var deleteRule = new ProcessStartInfo
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = "cmd.exe",
                Arguments = "/K " + string.Format("netsh advfirewall firewall delete rule name=\"{0}\"",
                                AppDomain.CurrentDomain.FriendlyName)
            };
            using (var proc = new Process())
            {
                proc.StartInfo = deleteRule;
                proc.Start();
                proc.WaitForExit(1000);
                //string output = proc.StandardOutput.ReadToEnd();

                //if (string.IsNullOrEmpty(output))
                //    output = proc.StandardError.ReadToEnd();
                //return output;
            }
            var procStartInfo = new ProcessStartInfo
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = "cmd.exe",
                Arguments = "/K " + string.Format(
                                "netsh advfirewall firewall add rule dir=in program=\"{0}\" name=\"{1}\" action=allow",
                                Application.ExecutablePath, AppDomain.CurrentDomain.FriendlyName)
            };
            using (var proc = new Process())
            {
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit(1000);
                //string output = proc.StandardOutput.ReadToEnd();

                //if (string.IsNullOrEmpty(output))
                //    output = proc.StandardError.ReadToEnd();
                //return output;
            }

            Application.Run(new MainForm());
        }
    }
}