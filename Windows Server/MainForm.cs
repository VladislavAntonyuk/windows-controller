﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using Database;

namespace Windows_Server
{
    public partial class MainForm : Form
    {
        private readonly BlockManager b = new BlockManager();

        //private IPAddress ip;

        private readonly SocketManager socket = new SocketManager(2048, 4199);
        private readonly List<User> users = new List<User>();
        private int clientCount;

        public MainForm()
        {
            InitializeComponent();

            socket.ClientConnected += socket_ClientConnected;
            socket.ClientDissconnected += socket_ClientDisconnected;
            socket.ReceivedMessage += socket_ReceivedMessage;
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIP)
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    IPAddressList.Items.Add(address.ToString());
            IPAddressList.SelectedIndex = 0;
        }

        public void EnterLog(string ms)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(EnterLog), ms);
                return;
            }
            statusStrip.Items.Clear();
            statusStrip.Items.Add("Clients:" + clientCount);
            statusStrip.Items.Add(ms);
        }

        private void Action(string ms)
        {
            switch (ms)
            {
                case "notepad":
                    Process.Start("notepad");
                    break;
                case "calc":
                    Process.Start("calc");
                    break;
                default:
                {
                    foreach (User VARIABLE in users)
                        socket.SendMessageToAllClients(VARIABLE.GetData(1, b));

                    users.First().UpdateData(1, "Player1_new", b);
                    b.AddBlock(1, users.First().Name);
                    var i = 0;
                    foreach (User VARIABLE in users)
                    {
                        socket.SendMessageToAClient(VARIABLE.GetData(1, b), socket._clients[i]);
                        i++;
                    }

                    b.RemoveBlock(1);
                    foreach (User VARIABLE in users)
                        socket.SendMessageToAllClients(VARIABLE.GetData(1, b));

                    break;
                }
            }
            socket.SendMessageToAllClients("done");
        }

        private void socket_ReceivedMessage(string Message, TcpClient FromClient)
        {
            EnterLog(Message);
            Action(Message);
        }

        private void socket_ClientDisconnected(TcpClient Client)
        {
            users.Remove(users.First(_ => _.Name == Client.Client.RemoteEndPoint.ToString()));
            clientCount--;
            EnterLog("Client disconnected");
        }

        private void socket_ClientConnected(TcpClient Client)
        {
            users.Add(new User
            {
                Name = Client.Client.RemoteEndPoint.ToString()
            });
            clientCount++;
            EnterLog("Client connected");
        }


        private void startServerButton_Click(object sender, EventArgs e)
        {
            socket.Host();
            socket.StartLisenClients();
            EnterLog("Server started");
            //IPAddress.TryParse(IPAddressList.SelectedItem.ToString(), out ip);
            //var buttons = new[] { portGroupBox }
            //      .SelectMany(g => g.Controls.OfType<RadioButton>()
            //                               .Where(r => r.Checked));
            //foreach (var c in buttons)
            //{
            //    switch (c.Text)
            //    {
            //        case "udp":


            //        break;
            //    case "tcp":

            //        break;
            //}
            //  }
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                socket.StopLisenClients();
            }
            finally
            {
                try
                {
                    socket.Stop();
                }
                finally
                {
                    Application.Exit();
                }
            }
        }

        private void startAcceptBtn_Click(object sender, EventArgs e)
        {
            socket.StartLisenClients();
        }

        private void stopAcceptBtn_Click(object sender, EventArgs e)
        {
            socket.StopLisenClients();
        }
    }
}