﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Windows_Server
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startServerButton = new System.Windows.Forms.Button();
            this.IPAddressList = new System.Windows.Forms.ComboBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.serverStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.startAcceptBtn = new System.Windows.Forms.Button();
            this.stopAcceptBtn = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // startServerButton
            // 
            this.startServerButton.Location = new System.Drawing.Point(12, 12);
            this.startServerButton.Name = "startServerButton";
            this.startServerButton.Size = new System.Drawing.Size(121, 23);
            this.startServerButton.TabIndex = 0;
            this.startServerButton.Text = "Start server";
            this.startServerButton.UseVisualStyleBackColor = true;
            this.startServerButton.Click += new System.EventHandler(this.startServerButton_Click);
            // 
            // IPAddressList
            // 
            this.IPAddressList.FormattingEnabled = true;
            this.IPAddressList.Location = new System.Drawing.Point(12, 41);
            this.IPAddressList.Name = "IPAddressList";
            this.IPAddressList.Size = new System.Drawing.Size(121, 21);
            this.IPAddressList.TabIndex = 3;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serverStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 81);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(267, 22);
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "statusStrip";
            // 
            // serverStatus
            // 
            this.serverStatus.Name = "serverStatus";
            this.serverStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // startAcceptBtn
            // 
            this.startAcceptBtn.Location = new System.Drawing.Point(139, 12);
            this.startAcceptBtn.Name = "startAcceptBtn";
            this.startAcceptBtn.Size = new System.Drawing.Size(121, 23);
            this.startAcceptBtn.TabIndex = 5;
            this.startAcceptBtn.Text = "Accept new";
            this.startAcceptBtn.UseVisualStyleBackColor = true;
            this.startAcceptBtn.Click += new System.EventHandler(this.startAcceptBtn_Click);
            // 
            // stopAcceptBtn
            // 
            this.stopAcceptBtn.Location = new System.Drawing.Point(139, 41);
            this.stopAcceptBtn.Name = "stopAcceptBtn";
            this.stopAcceptBtn.Size = new System.Drawing.Size(121, 23);
            this.stopAcceptBtn.TabIndex = 6;
            this.stopAcceptBtn.Text = "Decline new";
            this.stopAcceptBtn.UseVisualStyleBackColor = true;
            this.stopAcceptBtn.Click += new System.EventHandler(this.stopAcceptBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 103);
            this.Controls.Add(this.stopAcceptBtn);
            this.Controls.Add(this.startAcceptBtn);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.IPAddressList);
            this.Controls.Add(this.startServerButton);
            this.Name = "MainForm";
            this.Text = "Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button startServerButton;
        private ComboBox IPAddressList;
        private StatusStrip statusStrip;
        private ToolStripStatusLabel serverStatus;
        private Button startAcceptBtn;
        private Button stopAcceptBtn;
    }
}

