﻿using System.Data.Entity;

namespace Database
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public int Age { get; set; }
    }

    public class PlayerContext : DbContext
    {
        public PlayerContext() : base("Players")
        {
        }

        static PlayerContext()
        {
            System.Data.Entity.Database.SetInitializer(new PlayerDbInitializer());
        }

        public DbSet<Player> Players { get; set; }
    }

    public class PlayerDbInitializer : DropCreateDatabaseIfModelChanges<PlayerContext>
    {
        protected override void Seed(PlayerContext db)
        {
            db.Players.Add(new Player { Name = "1", Age = 10, Number = 100 });
            db.Players.Add(new Player { Name = "2", Age = 20, Number = 200 });
            db.Players.Add(new Player { Name = "3", Age = 30, Number = 300 });
            db.Players.Add(new Player { Name = "4", Age = 40, Number = 400 });
            db.SaveChanges();
        }
    }
}