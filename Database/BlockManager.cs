﻿using System.Collections.Generic;

namespace Database
{
    public class BlockManager
    {
        private readonly Dictionary<int, string> blocks = new Dictionary<int, string>();

        public bool IsBlock(int key, string username)
        {
            bool search = blocks.ContainsKey(key);
            if (search) return blocks[key] != username;
            return blocks.Count != 0;
        }

        public bool IsBlock(int key)
        {
            bool search = blocks.ContainsKey(key);
            if (search) return true;
            return blocks.Count != 0;
        }

        public void AddBlock(int elementIndex, string blockerName)
        {
            blocks.Add(elementIndex, blockerName);
        }

        public void RemoveBlock(int key)
        {
            blocks.Remove(key);
        }
    }
}