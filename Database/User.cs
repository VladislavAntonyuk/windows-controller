﻿using System.Data.Entity;
using System.Linq;

namespace Database
{
    public class User
    {
        public string Name { get; set; }

        public string GetData(int key, BlockManager b)
        {
            if (b.IsBlock(key, Name)) return null;
            using (var pc = new PlayerContext())
            {
                var players = pc.Players.Select(m => new { m.Name, m.Age, Position = m.Number });
                return players.FirstOrDefault()?.Name;
            }
        }

        public void UpdateData(int key, string data, BlockManager b)
        {
            if (b.IsBlock(key))
            {
                if (b.IsBlock(key, Name)) return;
                using (var d = new PlayerContext())
                {
                    Player player = d.Players.FirstOrDefault(p => p.Id == key);
                    if (player != null)
                    {
                        player.Name = data;
                        d.Entry(player).State = EntityState.Modified;
                        d.SaveChanges();
                    }
                }

                b.RemoveBlock(key);
            }
            else
            {
                b.AddBlock(key, Name);
                using (var d = new PlayerContext())
                {
                    Player player = d.Players.FirstOrDefault(p => p.Id == key);
                    if (player != null)
                    {
                        player.Name = data;
                        d.Entry(player).State = EntityState.Modified;
                        d.SaveChanges();
                    }
                }

                b.RemoveBlock(key);
            }
        }

        public void DeleteData(int key, BlockManager b)
        {
            if (b.IsBlock(key))
            {
                if (b.IsBlock(key, Name)) return;
                using (var d = new PlayerContext())
                {
                    Player player = d.Players.FirstOrDefault(p => p.Id == key);
                    if (player != null)
                    {
                        d.Players.Remove(player);
                        d.SaveChanges();
                    }
                }

                b.RemoveBlock(key);
            }
            else
            {
                b.AddBlock(key, Name);
                using (var d = new PlayerContext())
                {
                    Player player = d.Players.FirstOrDefault(p => p.Id == key);
                    if (player != null)
                    {
                        d.Players.Remove(player);
                        d.SaveChanges();
                    }
                }

                b.RemoveBlock(key);
            }
        }

        public void AddData(string data, BlockManager b)
        {
            using (var d = new PlayerContext())
            {
                d.Players.Add(new Player { Name = data });
                d.SaveChanges();
            }
        }
    }
}