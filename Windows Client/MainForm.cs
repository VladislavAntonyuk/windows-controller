﻿using System;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Windows_Client
{
    public partial class MainForm : Form
    {
        private readonly SocketManager socket = new SocketManager(2048, 4199);

        public MainForm()
        {
            InitializeComponent();
            socket.ReceivedMessage += socket_ReceivedMessage;
            socket.ConnectedServer += socket_ConnectedServer;
            socket.HostLost += socket_HostLost;
            socket.HostRefused += socket_HostRefused;
        }

        private void socket_HostRefused()
        {
            EnterLog("Server has been refused");
        }

        private void socket_HostLost()
        {
            MessageBox.Show("Server crashed!");
            Environment.Exit(0);
        }

        private void socket_ConnectedServer()
        {
            EnterLog("Successfully connected");
        }

        //void EnterLog(string ms)
        //{
        //    MessageBox.Show(ms);
        //}
        private void socket_ReceivedMessage(string Message, TcpClient FromClient)
        {
            EnterLog(Message);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            socket.Dissconnect();
            Application.Exit();
        }

        public void EnterLog(string ms)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(EnterLog), ms);
                return;
            }
            statusStrip.Items.Clear();
            statusStrip.Items.Add(ms);
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            if (socket.Connected)
            {
                socket.Dissconnect();
                connectBtn.Text = "Connect";
            }
            else
            {
                socket.Connect(sendString.Text);
                connectBtn.Text = "Disconnect";
            }
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            socket.SendMessageToHost(sendString.Text);
        }
    }
}